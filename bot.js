const Discord = require('discord.js');
const client = new Discord.Client();
const schema = require('./schema');
const meta = require('./meta/index')



client.on('ready', async e => {
    // this block is important 
    let messageID = null,
        isMessageAvailable = false;
    
    do {
        await client.channels.get('502093057685061633').fetchMessages({limit: 100, before: messageID})
        .then(msg => msg.forEach((m, i) => {

            if (msg.size === 0) return isMessageAvailable = false;
            isMessageAvailable = true;
            messageID = m.id;

            let links = m.content.match(/((http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?)/g);

            if (links) {
                links.forEach((link, index) => {
                  meta(link, (err, data) => {
                    if (err) return;
                    let data2save = new schema({
                      createdAt: m.createdAt,
                      meta: {
                        title: data.title,
                        image: data.image,
                        description: data.description,
                      },
                      link: link,
                      author: {
                          id: m.author.id,
                          username: m.author.username,
                          discriminator: m.author.discriminator,
                          avatar: m.author.avatar,
                      }
                    })
                    data2save.save()
                  })
                })
            }
        }))
    } while (isMessageAvailable);

})

client.login('NTIyNDA4NTkxMzA3NzAyMjcy.DxH4Rg.QSRNxSSwc99LfEUnCq-Ro02B6ps')