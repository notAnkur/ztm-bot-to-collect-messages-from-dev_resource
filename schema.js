const mongoose = require('mongoose');
mongoose.connect('mongodb://test:a123456@ds239681.mlab.com:39681/dev-resource-ztm', {useNewUrlParser: true});
// mongoose.connect('mongodb://localhost/dev-resources')

let schema = new mongoose.Schema({
    link: { type: String, required: true, unique: true },
    author: {
        id: { type: String, required: true, unique: false },
        username: { type: String, required: false, unique: false },
        discriminator: { type: String, required: true, unique: false },
        avatar: { type: String, required: true, unique: false },
    },
    createdAt: { type: Date, required: true, unique: false },
    meta: {
        title: { type: String, required: false, unique: false },
        image: { type: String, required: false, unique: false },
        description: { type: String, required: false, unique: false },
    }
}, {autoIndex: true})

module.exports = mongoose.model('links', schema);
