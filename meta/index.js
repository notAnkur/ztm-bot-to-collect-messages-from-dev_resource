const cheerio = require('cheerio');
const r = require('request');


module.exports = (url, cb) => {
    r(url, function (error, response, body) {
        if (error) return cb(error, null);
        
        let $ = cheerio.load(body);
        let metaData = {};
        
        metaData.title = $("meta[name='title']").attr("content") || $("meta[property='og:title']").attr("content");
        metaData.description = $("meta[name='description']").attr("content") || $("meta[property='og:description']").attr("content");
        metaData.image = $("meta[name='image']").attr("content") || $("meta[property='og:image']").attr("content");



        cb(null, metaData);
    });
}